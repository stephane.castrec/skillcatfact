import { ui } from 'ask-sdk-model';
import { getOne } from './utils/utils';


export const Constants = {
    messages: {
        welcome: () => {
            return 'Welcome on Cats Facts.'; 
        },
        next: () => {
            return 'Another one.';
        },
        error: () => {
            return 'Looks like a cat scared me. Try again.';
        },
        help: (): string => {
            return "With Cats Facts, have fun with a lot of cat facts. Do you want one ?";
        },
        not_supported: () => {
            return "This skill does not support this device";
        },
        reprompt: () => {
            return "Do you want another one ?"
        },
        bye: () => {
            return "See you soon in Cats Facts";
        }
    }
}