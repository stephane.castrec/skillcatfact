import * as request from 'request-promise-native';
import { getOne } from './utils/utils';

const url = "https://meowfacts.herokuapp.com/";
const forbidden = ["To unsubscribe from catfacts, reply the following code: tj3G5de$se"]
/*
[{"id":"271","fact":"http:\/\/www.chucknorrisfacts.fr\/img\/upload\/q48da669cd83d2.jpg","date":"1222288034","vote":"5494","points":"21503"}]
*/

export const getFact = async (): Promise<string> => {
    var options = {
        uri:url,
        json: true
    };

    let fact = await request.get(options);
    if (!fact.data || forbidden.includes(fact.data[0])) {
        fact = await getFact();
    }
    return fact.data[0];
}