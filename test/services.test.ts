'use strict';

import 'mocha';
import { assert } from 'chai';
import { getFact } from '../src/commons/services';

describe('Service tests', () => {
    it('getJoke', async () => {
        const j = await getFact();
        assert.isNotNull(j);
    });
});